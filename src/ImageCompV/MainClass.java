package ImageCompV;

import ij.ImagePlus;
import ij.io.Opener;
import ij.process.ColorProcessor;
import ij.process.ImageProcessor;

import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

import javax.imageio.ImageIO;

public class MainClass {

	public static void main(String[] args) {

		String fileAbsolutePath = "src/kep.png";
		String fileAbsolutePath2 = "src/bestpic.jpg";
		String fileAbsolutePath3 = "src/auto.jpg";
		String fileAbsolutePath4 = "src/kepfeher.jpg";
		String fileAbsolutePath5 = "src/it.jpg";
		String fileAbsolutePath6 = "src/auton.jpg";
		String fileAbsolutePath7 = "src/faces.jpg";
		String fileAbsolutePath8 = "src/facesb.jpg";
		String fileAbsolutePath9 = "src/feher.png";
		String fileAbsolutePath10 = "src/fekete.png";
		String fileAbsolutePath11 = "src/art1.jpg";
		String fileAbsolutePath12 = "src/art2.jpg";
		String fileAbsolutePath13 = "src/p1.jpg";
		String fileAbsolutePath14 = "src/p2.jpg";
		String fileAbsolutePath15 = "src/p3.jpg";
		String fileAbsolutePath16 = "src/p4.jpg";
		String fileAbsolutePath17 = "src/kauto.jpg";
		String fileAbsolutePath18 = "src/parlament.jpg";

		// imagej edge detector
		/*
		 * ColorProcessor ip; try { ip = new ColorProcessor(ImageIO.read(new
		 * File(fileAbsolutePath3))); ip.findEdges(); BufferedImage bimg =
		 * ip.getBufferedImage(); ImagePlus imagePlus = new
		 * ImagePlus("First Image", bimg); imagePlus.show(); } catch
		 * (IOException e1) { // TODO Auto-generated catch block
		 * e1.printStackTrace(); }
		 */

		CannyEdgeDetector detector = new CannyEdgeDetector();

		BufferedImage img = null;
		try {
			img = ImageIO.read(new File(fileAbsolutePath15));
			ImagePlus imagePlus2 = new ImagePlus("First Image", img);
			imagePlus2.show();
			// adjust its parameters as desired
			detector.setLowThreshold(0.6f);
			detector.setHighThreshold(7.5f);
			// apply it to an image
			detector.setSourceImage(img);
			detector.process();
			BufferedImage edges = detector.getEdgesImage();

			int[][] pixels = new int[edges.getWidth()][edges.getHeight()];
			

			StringBuffer s = new StringBuffer();

			for (int i = 0; i < edges.getWidth(); ++i) {
				for (int j = 0; j < edges.getHeight(); ++j) {
					pixels[i][j] = edges.getRGB(i, j);
					if(pixels[i][j]!=-1){
						s.append("  ");
					}
					else{
						s.append(pixels[i][j]+" ");
					}
				}
				s.append("\n");
			}

			/*int firstX = -1;
			int firstY = -1;
			for (int i = 0; i < edges.getWidth(); ++i) {
				for (int j = 0; j < edges.getHeight(); ++j) {
					pixels[i][j] = edges.getRGB(i, j);
					if (firstX != -1 && pixels[i][j] == -1) {
						firstX = i;
						firstY = j;
					}
				}
			}*/

			FileWriter output = null;
			try {
				output = new FileWriter("ki.txt");
				BufferedWriter writer = new BufferedWriter(output);
				writer.write(s.toString());
			} catch (Exception e) {
				throw new RuntimeException(e);
			} finally {
				if (output != null) {
					try {
						output.close();
					} catch (IOException e) {
						// Ignore issues during closing
					}
				}
			}

			/*
			 * int borderX = edges.getWidth(); int borderY = edges.getHeight();
			 * int blackx; int blacky; int[][] helpMatrix = new int[3][3]; while
			 * (true) {
			 * 
			 * if (firstX + 1 < borderX) { helpMatrix[1][2] = pixels[firstX +
			 * 1][firstY];
			 * 
			 * if ((firstY - 1) >= 0) { helpMatrix[0][2] = pixels[firstX +
			 * 1][firstY - 1]; } if ((firstY + 1) < borderY) { helpMatrix[2][2]
			 * = pixels[firstX + 1][firstY + 1]; }
			 * 
			 * } if ((firstX - 1) >= 0) { helpMatrix[1][0] = pixels[firstX -
			 * 1][firstY]; if ((firstY - 1) >= 0) { helpMatrix[0][0] =
			 * pixels[firstX - 1][firstY - 1]; } if ((firstY + 1) < borderY) {
			 * helpMatrix[2][0] = pixels[firstX - 1][firstY + 1]; }
			 * 
			 * } if (firstY - 1 >= 0) { helpMatrix[0][1] = pixels[firstX][firstY
			 * - 1]; } if (firstY + 1 < borderY) { helpMatrix[2][1] =
			 * pixels[firstX][firstY + 1]; }
			 * 
			 * if(helpMatrix[1][2]==-1){
			 * 
			 * }
			 * 
			 * }
			 */
			
			ImagePlus picNew = new ImagePlus("Uj kep",edges);
			picNew.show();

		} catch (IOException e) {
		}

		ImageDesc desc = new ImageDesc(fileAbsolutePath3, fileAbsolutePath6);
		// desc.theTwoImageEntorpyValue();//valamiert kiakad az egyszeru
		// szineknel
		// hiba ellenteteseknel

		try {
			// Types: Gray=0 , HSB(hue saturation brightness) = 1,Blue = 2,
			// Green.= 3, Red= 4 , RGB= 5
			// System.out.println("The difference: "+desc.normalizationOfTwoSameTypeOfHistogram(0));
			// //<-nem mukodik jol
			// if its is 0 Than They Are equal
			// System.out.println("\nEuclidian Distance Of Color Quantization "+desc.colorQuantizationEuclidianDistance());//egyszeru
			// szineknel hiba
			/*
			 * File f = new File(fileAbsolutePath3); ColorProcessor image = new
			 * ColorProcessor(ImageIO.read(f)); for (double d :
			 * desc.getHistOfaRoidImage(image, image.getWidth()/2, 0,
			 * image.getWidth()/4, image.getHeight(), 5)) {
			 * System.out.print(d+" "); }
			 */
			desc.openImageEdgeDetect();
			desc.normalizationOfTwoSameTypeOfHistogramAndDividThem(4, 4);
		} catch (IOException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}

	}

}
